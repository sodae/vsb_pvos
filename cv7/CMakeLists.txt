cmake_minimum_required(VERSION 3.8)
project(cv7)

set(CMAKE_CXX_STANDARD 14)

set(SOURCE_FILES main.cpp)
add_executable(cv7 ${SOURCE_FILES})
target_link_libraries(cv7 pthread)
target_link_libraries(cv7 rt)
