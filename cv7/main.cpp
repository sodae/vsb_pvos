#include <iostream>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <unordered_map>
#include <mqueue.h>
#include <sys/msg.h>
#include <semaphore.h>


#define GOODS_PER_WORKER 100000

#define WORKERS 10

#define MAX_VYROBKU 1000

struct prepravka {
    int pocet = 0;
    int vyrobky[MAX_VYROBKU];
};

int workersQueue;
mqd_t consumerQueue;

msgbuf normalMsg = {1, ""};
int normalSize = sizeof(msgbuf) - sizeof(long);

#define TRY(x, y) { if (y < 0) { printf("code %d\n", y); perror(x); exit(254); } }

void task1_worker(int pid, prepravka *crate) {
    msgbuf msg;
    for (int i = 0; i < GOODS_PER_WORKER; i++) {
        TRY("w-msgrcv", msgrcv(workersQueue, &msg, normalSize, 0, 0));
        int count = crate->pocet;
        crate->vyrobky[count++] = pid;
        crate->pocet = count;
        if (count == MAX_VYROBKU) {
            TRY("w-MQ_SEND", mq_send(consumerQueue, "", 1, 0));
        } else {
            TRY("w-msgend", msgsnd(workersQueue, &normalMsg, normalSize, 0));
        }
    }
}

void task1_consumer(prepravka *crate) {
    int maxCratesPerDay = (GOODS_PER_WORKER * WORKERS) / MAX_VYROBKU;

    TRY("c-msgsnd-i", msgsnd(workersQueue, &normalMsg, normalSize, 0));

    char msg[10];
    for (int cratesCount = 0; cratesCount < maxCratesPerDay; cratesCount++) {
        TRY("c-mq_receive", mq_receive(consumerQueue, msg, 10, nullptr));

        std::unordered_map<int, int> statistics;
        for (int i = 0; i < MAX_VYROBKU; i++) {
            int pid = crate->vyrobky[i];
            statistics[pid]++;
        }

        for (auto row: statistics) {
            std::cout << row.first << ": " << row.second << std::endl;
            if (row.first == 0) exit(-2);
        }

        crate->pocet = 0;
        memset(crate->vyrobky, 0, sizeof(crate->vyrobky));

        std::cout << "----------------------" << std::endl;

        TRY("c-msgsnd", msgsnd(workersQueue, &normalMsg, normalSize, 0));
    }
}

void task1() {

    struct mq_attr attr;
    /* initialize the queue attributes */
    attr.mq_flags = 0;
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = 1024;
    attr.mq_curmsgs = 0;

    workersQueue = msgget(0xcafecafe, IPC_CREAT | 0644);
    consumerQueue = mq_open("/fila", O_CREAT | O_RDWR, 0644, &attr);

    TRY("init-W", workersQueue);
    TRY("init-C", consumerQueue);

    auto *data = (prepravka *) mmap(
            nullptr,
            sizeof(prepravka),
            PROT_READ | PROT_WRITE,
            MAP_SHARED | MAP_ANONYMOUS,
            -1,
            0
    );

    int childId = 1;
    for (int i = 0; i < WORKERS && childId; i++) {
        childId = fork();
    }

    if (childId) {
        task1_consumer(data);
        wait(NULL);

        mq_close(consumerQueue);
        msgctl(workersQueue, IPC_RMID, 0);
    } else {
        task1_worker(getpid(), data);
    }
}


const static int MAX_MESSAGE_SIZE = 12;
const static int MAX_MESSAGES_IN_QUEUE = 12;

struct Message {
    int type;
    char message[MAX_MESSAGE_SIZE];
};

struct MessageQueue {
    Message messages[MAX_MESSAGES_IN_QUEUE];
    int count = 0;

    int countOf(int type) {
        int c = 0;
        for (int i = 0; i < count; i++) {
            if (type == 0 || type == messages[i].type) {
                c++;
            }
        }
        return c;
    }

    void pop(int type, Message *msg) {
        for (int i = 0; i < count; i++) {
            if (type == 0 || type == messages[i].type) {
                memcpy(msg, &messages[i], sizeof(Message));
                memcpy(
                        messages + i,
                        messages + i + 1,
                        sizeof(Message) * (count - i - 1)
                );
                count--;
                break;
            }
        }
    }

    void add(const Message msg) {
        memcpy(&messages[count], &msg, sizeof(Message));
        count++;
    }
};

class MyQueue {

private:

    const static int ALL_MESSAGES = 0;
    const static int MAX_TYPES = 8;
    sem_t *semaphores[MAX_TYPES];
    MessageQueue* messageQueue;

public:

    MyQueue() {
        messageQueue = (MessageQueue *) mmap(
                nullptr,
                sizeof(MessageQueue),
                PROT_READ | PROT_WRITE,
                MAP_SHARED | MAP_ANONYMOUS,
                -1,
                0
        );
        for (int i = 0; i <= MAX_TYPES; i++) {
            semaphores[i] = (sem_t *) mmap(
                    nullptr,
                    sizeof(sem_t),
                    PROT_READ | PROT_WRITE,
                    MAP_SHARED | MAP_ANONYMOUS,
                    -1,
                    0
            );
            TRY("init-sem", sem_init(semaphores[i], 1, 1));
        }
    }

    virtual ~MyQueue() {
        for (int i = 0; i <= MAX_TYPES; i++) {
            sem_destroy(semaphores[i]);
            munmap(semaphores[i], sizeof(sem_t));
        }
        munmap(messageQueue, sizeof(MessageQueue) * MAX_MESSAGES_IN_QUEUE);
    }

    void post(const Message &message) {
        int type = message.type;
        if (type == ALL_MESSAGES) {
            throw "type zero is not allowed";
        }
        sem_wait(semaphores[type]);
        sem_wait(semaphores[ALL_MESSAGES]);

        while (messageQueue->count == MAX_MESSAGE_SIZE) {
            sem_post(semaphores[ALL_MESSAGES]);
            sem_post(semaphores[type]);
            sem_wait(semaphores[type]);
            sem_wait(semaphores[ALL_MESSAGES]);
        }

        messageQueue->add(message);

        sem_post(semaphores[ALL_MESSAGES]);
        sem_post(semaphores[type]);
    }

public:
    void receive(int type, Message *msg) {
        sem_wait(semaphores[type]);
        while (messageQueue->countOf(type) == 0) {
            sem_post(semaphores[type]);
            sem_wait(semaphores[type]);
        }

        messageQueue->pop(type, msg);
        sem_post(semaphores[type]);
    }
};

void task2() {
    MyQueue mq;

    int childId = 1;
    for (int i = 0; i < 10 && childId; i++) {
        childId = fork();
    }

    if (!childId) {
        for (int i = 0; i < 5; i++) {
            Message message = {1, "Trpa"};
            mq.post(message);
            Message message2 = {2, "Ales"};
            mq.post(message2);
        }
    } else {
        Message message;
        while (true) {
            mq.receive(0, &message);
            printf("received: type(%d) message(%s)\n", message.type, message.message);
        }
    }
}

int main() {
    //task1();
    task2();
    return 0;
}