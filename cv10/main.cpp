#include <iostream>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <poll.h>
#include <vector>
#include <algorithm>
#include <csignal>
#include <arpa/inet.h>
#include <cstring>

#define TRY(x, y) { if (y < 0) { printf("code %d\n", y); perror(x); exit(254); } }

/*

Upravte soktetovy server pro "chat" ve variante select nebo poll.
Rodic vytvori dva potomky, kterym bude pres 2xsocketpair predavat nove klienty.
Potomci budou prijimat zpravy jen od sudych nebo lichych deskriptoru, ale stale posilat vsem.

Rodic resi accept, posle obema potomkum. Potomci maji select nebo poll jen na lichych/sudych
deskriptorech.
 */

struct ConnectionLetter {
    int fd;
    bool read;
};

void sendNewFd(int receiverFd, ConnectionLetter letter) {
    msghdr zprava;
    zprava.msg_name = NULL;
    zprava.msg_namelen = 0;
    zprava.msg_flags = 0;

    iovec io = {&letter.read, sizeof(bool)};
    zprava.msg_iov = &io;
    zprava.msg_iovlen = 1; // doporuceno nemit prazdne zpravy
    char data[CMSG_SPACE(sizeof(int))];
    zprava.msg_control = data;
    zprava.msg_controllen = sizeof(data);
    cmsghdr *cmsg = CMSG_FIRSTHDR(&zprava);
    cmsg->cmsg_level = SOL_SOCKET;
    cmsg->cmsg_type = SCM_RIGHTS;
    cmsg->cmsg_len = CMSG_LEN(sizeof(int));
    *(int *) CMSG_DATA(cmsg) = letter.fd;

    printf("sndmsg: %d, %d\n", letter.fd, letter.read);
    sendmsg(receiverFd, &zprava, 0);
}

void receiveNewFd(int senderFd, ConnectionLetter& letter) {
    msghdr zprava;
    zprava.msg_name = NULL;
    zprava.msg_namelen = 0;
    zprava.msg_flags = 0;
    bool read;
    iovec io = {&read, sizeof(bool)};
    zprava.msg_iov = &io;
    zprava.msg_iovlen = 1; // doporuceno nemit prazdne zpravy
    char data[CMSG_SPACE(sizeof(int))];
    zprava.msg_control = data;
    zprava.msg_controllen = sizeof(data);

    recvmsg(senderFd, &zprava, 0);

    cmsghdr *cmsg = CMSG_FIRSTHDR(&zprava);
    int fd = *(int *) CMSG_DATA(cmsg);

    letter.fd = fd;
    letter.read = read;

    printf("rcvmsg: %d, %d\n", letter.fd, letter.read);
}


void dispacher(int serverSocketFd, std::vector<int> workersFds) {

    std::vector<pollfd> polls;
    polls.push_back({serverSocketFd, POLLIN});

    unsigned int counter = 0;

    while (true) {
        TRY("poll", poll(polls.data(), polls.size(), 1000));
        for (auto &poll: polls) {
            if (!(poll.revents & POLLIN))
                continue;

            if (poll.fd == serverSocketFd) { // server socket
                printf("new client\n");
                sockaddr_in peeradr{};
                socklen_t len_adr = sizeof(peeradr);
                int newsock = accept(serverSocketFd, (sockaddr *) &peeradr, &len_adr);

                int i = 0;
                for (auto workerFd: workersFds) {
                    sendNewFd(workerFd, {newsock, i == counter});
                    i++;
                }

                counter = (counter + 1) % (unsigned int) workersFds.size();
            }
        }
    }
}

void worker(int pairSocket) {

    std::vector<pollfd> readPoll;
    std::vector<pollfd> sendPoll;
    readPoll.push_back({pairSocket, POLLIN});

    while (true) {
        TRY("poll", poll(readPoll.data(), readPoll.size(), 1000));
        for (auto &poll: readPoll) {
            if (poll.revents & POLLHUP) {
                poll.fd = -1;
                continue;
            }

            if (!(poll.revents & POLLIN))
                continue;

            if (poll.fd == pairSocket) {
                ConnectionLetter letter;
                receiveNewFd(poll.fd, letter);
                printf("%d new client: %d (%d)\n", getpid(), letter.fd, letter.read);
                if (letter.read) {
                    readPoll.push_back({letter.fd, POLLIN});
                }
                sendPoll.push_back({letter.fd, POLLIN});

            } else {
                printf("%d: new message\n", getpid());
                char message[255];
                int size = read(poll.fd, message, sizeof(message));
                TRY("read", size);

                std::string msg(message);
                if (msg.find("!close") != std::string::npos) {
                    printf("%d: close %d\n", getpid(), poll.fd);
                    close(poll.fd);
                    poll.fd = -1;
                }

                for (auto broadcastPoll: sendPoll) {
                    if (broadcastPoll.fd == -1) { // server socket
                        continue;
                    }
                    int writeRes = write(broadcastPoll.fd, message, size * sizeof(char));

                    if (writeRes < 0) {
                        broadcastPoll.fd = -1;
                    }
                }
            }
        }

        auto pollIterator = readPoll.begin();
        for (int i = 0; i < readPoll.size(); ++i) {
            if (pollIterator->fd == -1) {
                pollIterator = readPoll.erase(pollIterator);
            } else {
                pollIterator++;
            }
        }
    }
}

int main() {

    int HOW_MANY_WORKERS = 2;

    sockaddr_in inadr{};
    inadr.sin_family = AF_INET;
    inadr.sin_port = htons(12345);
    inadr.sin_addr.s_addr = INADDR_ANY;

    int serverSocketFd = socket(AF_INET, SOCK_STREAM, 0);

    int opt = 1;
    TRY("reuse_port", setsockopt(serverSocketFd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)));
    TRY("bind", bind(serverSocketFd, (sockaddr *) &inadr, sizeof(inadr)));
    TRY("listen", listen(serverSocketFd, 100));

    std::vector<int> fds;
    for (int i = 0; i < HOW_MANY_WORKERS; ++i) {
        int pair[2];
        socketpair(AF_UNIX, SOCK_STREAM, 0, pair);
        fds.push_back(pair[0]);
        if (fork() == 0) worker(pair[1]);
    }

    dispacher(serverSocketFd, fds);

    close(serverSocketFd);
    return 0;
}

