#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <poll.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <sys/param.h>
/**
 * 1. Pri komunikaci s pouzitim signalu SIGIO vyuzijte obsluhu signalu rozsirenou o siginfo_t.
  Overte, kdyz budou dva potomci pomalu posilat rodici data pres dve roury,
  ze lze rozlisit zdroj (file descriptor) dat.

2. Pri komunikaci pres aio overte prevzeti dat pomoci vlakna (man sigevent / SIGEV_THREAD)

3. Overte, zda je omezen pocet vlaken, ktera se mohou vytvorit (viz prvni cviceni).
  Potomek pozila data rodici po 1 bajtu, napr. po 1 ms. (Overte vlakna pomoci pthread_self).

4. Vyzkousejte, zda je mozno predat aiocb do obsluhy signalu pres siginfo_t
  a polozku sigval(_t).
  Overte s vice potomky a vice rourami, min 2.
 */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <sys/param.h>
#include <aio.h>
#include <pthread.h>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"

void signal2(int signum, siginfo_t *info, void *ptr) {
    printf("mam signal %d od %d\n", signum, info->si_fd);
    char buf[256];
    read(info->si_fd, buf, sizeof(buf));
}

#define PIPE_LEN 2

void forkLife(int *pipe) {
    char buf[128];
    unsigned int cas = (unsigned int) rand() % 2000;
    //printf("(%d) bude cekat %d ms\n", getpid(), cas);
    usleep(cas * 5000);
    sprintf(buf, "(%d) %d\n", getpid(), rand() % 100);
    write(pipe[1], buf, strlen(buf));
    printf("(%d) odeslal\n", getpid());
}

int task_1() {
    int pipes[PIPE_LEN][2];
    for (int i = 0; i < PIPE_LEN; i++) {
        pipe(pipes[i]);
    }

    int i = 0;
    int pid = -1;
    for (i = 0; i < PIPE_LEN; i++) {
        if ((pid = fork()) == 0) break;
        printf("PID: %d\n", pid);
    }
    if (pid == 0) {
        // let only one pipe per fork
        for (int j = 0; j < PIPE_LEN; j++) {
            if (j == i) continue; // it is pipe for the fork
            close(pipes[j][0]);
            close(pipes[j][1]);
        }
        srand((unsigned int) getpid());
        while (1) {
            forkLife(pipes[i]);
        }
    } else {
        for (int j = 0; j < PIPE_LEN; j++) {
            int handle = pipes[j][0];
            struct sigaction sa;
            sa.sa_sigaction = signal2;
            sa.sa_flags = SA_SIGINFO;
            sigemptyset(&sa.sa_mask);

            sigaction(SIGIO, &sa, NULL);

            fcntl(handle, F_SETFL, fcntl(handle, F_GETFL) | O_ASYNC);
            fcntl(handle, F_SETOWN, getpid());
            fcntl(handle, F_SETSIG, SIGIO);
        }

        while (1) {
            printf("nuda...\n");
            sleep(1);
        }
    }
}

void task_2_happy_handler(sigval value) {
    auto block = (aiocb *) value.sival_ptr;
    ssize_t size = aio_return(block);
    char *data = (char *) block->aio_buf;
    data[size] = '\0';
    printf("thread %ld (%d): %s", pthread_self(), size, data);
    aio_read(block);
}

int task_2() {
    int dateLength = 256;
    char data[dateLength];
    aiocb aio;
    aio.aio_fildes = STDIN_FILENO;
    aio.aio_offset = 0;
    aio.aio_reqprio = 0;
    aio.aio_buf = data;
    aio.aio_nbytes = dateLength;
    aio.aio_sigevent.sigev_notify = SIGEV_THREAD;
    aio.aio_sigevent.sigev_notify_function = task_2_happy_handler;
    aio.aio_sigevent.sigev_notify_attributes = nullptr;
    aio.aio_sigevent.sigev_value.sival_ptr = &aio;
    aio.aio_lio_opcode = 0;

    printf("main pthread %ld\n", pthread_self());

    aio_read(&aio);

    while (true) {
        sleep(1);
    }
}

void poorForkLife(int *pipe) {
    char buf[128];
    unsigned int cas = (unsigned int) rand() % 2000;
    usleep(cas);
    sprintf(buf, "(%d) %d\n", getpid(), rand() % 100);
    write(pipe[1], buf, strlen(buf));
}

int task_3() {

    int pipes[PIPE_LEN][2];
    for (int i = 0; i < PIPE_LEN; i++) {
        pipe(pipes[i]);
    }

    int i = 0;
    int pid = -1;
    for (i = 0; i < PIPE_LEN; i++) {
        if ((pid = fork()) == 0) break;
        printf("PID: %d\n", pid);
    }
    if (pid == 0) {
        // let only one pipe per fork
        for (int j = 0; j < PIPE_LEN; j++) {
            if (j == i) continue; // it is pipe for the fork
            close(pipes[j][0]);
            close(pipes[j][1]);
        }
        srand((unsigned int) getpid());
        while (1) {
            poorForkLife(pipes[i]);
        }
    } else {
        aiocb cbs[PIPE_LEN];
        for (int j = 0; j < PIPE_LEN; j++) {
            int handle = pipes[j][0];

            int dateLength = 256;
            char data[dateLength];
            aiocb aio;
            aio.aio_fildes = handle;
            aio.aio_offset = 0;
            aio.aio_reqprio = 0;
            aio.aio_buf = data;
            aio.aio_nbytes = dateLength;
            aio.aio_sigevent.sigev_notify = SIGEV_THREAD;
            aio.aio_sigevent.sigev_notify_function = task_2_happy_handler;
            aio.aio_sigevent.sigev_notify_attributes = nullptr;
            aio.aio_sigevent.sigev_value.sival_ptr = &aio;
            aio.aio_lio_opcode = 0;
            cbs[j] = aio;

            aio_read(&aio);


            while (1) {
                printf("nuda...\n");
                sleep(1);
            }
        }

    }

    // nikdy nastesti neskonci :)
}

void task_4_annoyed_handler(int signum, siginfo_t *siginfo, void *data) {
    task_2_happy_handler(siginfo->si_value);
}

void task_4() {

    int pipes[PIPE_LEN][2];
    for (int i = 0; i < PIPE_LEN; i++) {
        pipe(pipes[i]);
    }

    int i = 0;
    int pid = -1;
    for (i = 0; i < PIPE_LEN; i++) {
        if ((pid = fork()) == 0) break;
        printf("PID: %d\n", pid);
    }
    if (pid == 0) {
        // let only one pipe per fork
        for (int j = 0; j < PIPE_LEN; j++) {
            if (j == i) continue; // it is pipe for the fork
            close(pipes[j][0]);
            close(pipes[j][1]);
        }
        srand((unsigned int) getpid());
        while (1) {
            poorForkLife(pipes[i]);
        }
    } else {
        struct sigaction signal;
        signal.sa_sigaction = task_4_annoyed_handler;
        signal.sa_flags = SA_SIGINFO | SA_RESTART;
        sigemptyset(&signal.sa_mask);
        sigaction(SIGIO, &signal, NULL);

        aiocb cbs[PIPE_LEN];
        for (int j = 0; j < PIPE_LEN; j++) {
            int handle = pipes[j][0];

            int dateLength = 256;
            char data[dateLength];
            aiocb aio;
            aio.aio_fildes = handle;
            aio.aio_offset = 0;
            aio.aio_reqprio = 0;
            aio.aio_buf = data;
            aio.aio_nbytes = dateLength;
            aio.aio_sigevent.sigev_notify = SIGEV_SIGNAL;
            aio.aio_sigevent.sigev_signo = SIGIO;
            aio.aio_sigevent.sigev_notify_attributes = nullptr;
            aio.aio_sigevent.sigev_value.sival_ptr = &aio;
            aio.aio_lio_opcode = 0;
            cbs[j] = aio;

            aio_read(&aio);


            while (1) {
                printf("nuda...\n");
                sleep(1);
            }
        }

    }

}


int main() {
    //task_1();
    //task_2();
    //task_3();
    task_4();
}


#pragma clang diagnostic pop