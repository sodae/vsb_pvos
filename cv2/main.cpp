#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <vector>

void awesome_signal_handler(int sig) {
    std::cout << "Signal:" << sig << std::endl;
}

/**
 * 1. Overte, jak zasahuji signaly do chovani funkce scanf (sigaction bez SA_RESTART)
 */
void task_1() {
    struct sigaction signal;
    signal.sa_handler = awesome_signal_handler;
    signal.sa_flags = 0;
    sigemptyset(&signal.sa_mask);
    sigaction(SIGALRM, &signal, NULL);

    int str;
    scanf("%d", &str);
    printf("r=%d errno=%d (%s)\n", str, errno, strerror(errno));

    alarm(1);
    sleep(2);

    scanf("%d", &str);
    printf("r=%d errno=%d (%s)\n", str, errno, strerror(errno));

    /**
1
r=1 errno=0 (Success)
22222222222Signal:14
22222222222222222222
r=-1 errno=34 (Numerical result out of range)
     */
}


/**
// pro dalsi priklady radove tisice potomku
2. Udrzovat seznam potomku ve vector<int>. Posilejte jim nahodny signaly USR1 a URS2.
  Potomek na USR1 skonci korektne, na USR2 havarie.
3. Na ukonceni potomku reagujte zachycenim signalu CHLD.
  - sledujte narustani zombie
  - vyreste ztraceni signalu (pocitat ztracene signaly)
 */

int countOfDeadChildren = 0;
int stillSleep = 0;

void usr_signal_handler(int sig) {
    std::cout << "Signal:" << sig << ":" << strsignal(sig) << std::endl;
    if (sig == SIGUSR1) {
        exit(EXIT_SUCCESS);
    } else if (sig == SIGUSR2) {
        exit(EXIT_FAILURE);
    } else if (sig == SIGCHLD) {
        countOfDeadChildren++;
    } else if (sig == SIGALRM) {
        stillSleep = 0;
    }
}

void regHandler() {
    struct sigaction signal;
    signal.sa_handler = usr_signal_handler;
    signal.sa_flags = 0;
    sigemptyset(&signal.sa_mask);
    sigaction(SIGUSR1, &signal, NULL);
    sigaction(SIGUSR2, &signal, NULL);
    sigaction(SIGCHLD, &signal, NULL);
    sigaction(SIGALRM, &signal, NULL);
}

void boredFork() {
    while (true) {
        usleep(100);
    }
}

void task_2_3() {
    int sizeOfForks = 1000;
    regHandler();

    std::vector<int> forksIds;
    for (int i = 0; i < sizeOfForks; i++) {
        int pid = fork();
        if (pid == 0) {
            boredFork();
        }
        forksIds.push_back(pid);
    }

    for (auto forkId: forksIds) {
        kill(forkId, rand() % 2 ? SIGUSR1 : SIGUSR2);
    }

    alarm(3);
    stillSleep = 1;
    while(stillSleep) {
        sleep(100);
    }

    int exited = 0;
    for (auto forkId: forksIds) {
        int status;
        waitpid(forkId, &status, 0);
        exited += WIFEXITED(status);
        printf("%d: exited, status=%d\n", forkId, WEXITSTATUS(status));
    }

    std::cout << "Exited by waitpid: " << exited << std::endl;
    std::cout << "Deaths by signal: " << countOfDeadChildren << std::endl;

    /**
(long log)
Exited by waitpid: 1000
Deaths by signal: 553
     */
}

int main() {
    //task_1();
    task_2_3();
    return 0;
}