#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <vector>

int const IS_CHILD = 0;

void cleanForks() {
    int ret;
    while ((ret = wait(NULL)) != -1) {
        printf("wait %d\n", ret);
    }
}

/**
 * 1. Vytvarejte procesy kazdych 10 ms, prubezne uklizejte zombie
  viz zdrojak, pozor na waitpid a WNOHANG
 */

void task_1() {
    while (true) {
        cleanForks();

        int pid;

        pid = fork();
        if (pid == IS_CHILD) {
            break;
        }
        printf("fork: %d\n", pid);

        pid = fork();
        if (pid == IS_CHILD) {
            break;
        }
        printf("fork: %d\n", pid);

        usleep(10);
    }
    usleep(10);
}


void crazyForkFunction() {
    if (rand() % 2 == 0) {
        printf("BOOM!\n");
        int a[0] = {};
        a[1] = 0xdeadbeef; // overflow
    }
}

/**
 * 2. potomci budou koncit korekte s navratovym kodem, nebo nahodne nejak havarie (deleni 0, spatny pointer)
  pomoci wait rozlisujte, jak kdo skoncil
 */
void task_2() {
    int pid;
    for (int i = 0; i < 10; i++) {
        pid = fork();
        if (pid == IS_CHILD) {
            srand(time(0)); // :)) http://imgs.xkcd.com/comics/random_number.png
            crazyForkFunction();
            exit(0);
        } else {
            printf("fork: %d\n", pid);
        }
        usleep(500000);
    }

    cleanForks();
}

bool letsDie = 0;

void *my_awesome_thread(void *_) {
    while(true) {
        sleep(5);
        if (letsDie) break;
    }
}

/**
 * 3. Napiste testovaci program, ktery overi, kolik vlaken muze mit soucasne proces.
 */
void task_3() {
    std::vector<pthread_t> th_ids;

    while (true) {
        pthread_t th_id;
        int created = pthread_create(&th_id, NULL, my_awesome_thread, NULL);
        if (created != 0) {
            break;
        }
        th_ids.push_back(th_id);
    }

    std::cout << "size: " << th_ids.size() << std::endl;

    letsDie = true;
    for (auto th_id: th_ids) {
        pthread_join(th_id, NULL);
    }
    std::cout << std::endl << "--- DONE ---" << std::endl;
    letsDie = false;
}

void *boring_thread(void *_) {
    rand();
}

/**
 * 4. Napiste testovaci program, ktery overi, kolik vlaken postupne muze proces vytvorit
  (bez join a detach)
  Po zavadeni detach nebude limit.
 */
void task_4() {
    std::vector<pthread_t> th_ids;

    while (true) {
        pthread_t th_id;
        int created = pthread_create(&th_id, NULL, boring_thread, NULL);
        if (created != 0) {
            break;
        }
        th_ids.push_back(th_id);
    }

    std::cout << "size: " << th_ids.size() << std::endl;

}

int main() {
    //task_1();
    //task_2();
    //task_3();
    task_4();
    return 0;
}