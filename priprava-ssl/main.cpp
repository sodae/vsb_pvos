#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <vector>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/select.h>
#include <sys/param.h>
#include <poll.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <algorithm>
#include <csignal>
#include <arpa/inet.h>
#include <cstring>
#include <termios.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <unordered_map>
#include <mqueue.h>
#include <sys/msg.h>
#include <semaphore.h>

#include <openssl/rsa.h>       /* SSLeay stuff */
#include <openssl/x509.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#define TRY(x, y) { if (y < 0) { printf("code %d\n", y); perror(x); exit(254); } }
#define CHK_NULL(x) if ((x)==NULL) exit (1)
#define CHK_SSL(err) if ((err)==-1) { ERR_print_errors_fp(stderr); exit(2); }

#define HOME "./crts/"
#define CERTF  HOME "myserver.crt"
#define KEYF  HOME  "mypriv.pem"

class Client {
protected:
    int fd = -1;

public:
    Client(int fd) : fd(fd) {}

    virtual int mread(void *buf, int num) {
        return read(fd, buf, num);
    }

    virtual int mwrite(void *buf, int num) {
        return write(fd, buf, num);
    }

    virtual ~Client() {
        close(fd);
    }
};

class SSLClient : public Client {

    SSL* ssl = nullptr;

public:
    SSLClient(int fd, SSL* ssl) : Client(fd), ssl(ssl) {}

    int mread(void *buf, int num) override {
        int size = SSL_read(ssl, buf, num);
        CHK_SSL(size);
        return size;
    }

    int mwrite(void *buf, int num) override {
        int size = SSL_write(ssl, buf, num);
        CHK_SSL(size);
        return size;
    }

    virtual ~SSLClient() {
        SSL_free(ssl);
    }
};

class BothServer {

    SSL_CTX* initSSL() {
        const SSL_METHOD* meth;
        SSL_CTX* ctx;

        /* SSL preliminaries. We keep the certificate and key with the context. */

        SSL_load_error_strings();
        SSLeay_add_ssl_algorithms();
        meth = SSLv23_server_method();
        ctx = SSL_CTX_new(meth);
        if (!ctx) {
            ERR_print_errors_fp(stderr);
            exit(2);
        }

        if (SSL_CTX_use_certificate_file(ctx, CERTF, SSL_FILETYPE_PEM) <= 0) {
            ERR_print_errors_fp(stderr);
            exit(3);
        }
        if (SSL_CTX_use_PrivateKey_file(ctx, KEYF, SSL_FILETYPE_PEM) <= 0) {
            ERR_print_errors_fp(stderr);
            exit(4);
        }

        if (!SSL_CTX_check_private_key(ctx)) {
            fprintf(stderr, "Private key does not match the certificate public key\n");
            exit(5);
        }
        return ctx;
    }

    Client* acceptSSLClient(int clientFd, SSL_CTX* ctx) {
        int err;
        int sd = clientFd;
        SSL* ssl = SSL_new(ctx);

        CHK_NULL(ssl);
        SSL_set_fd(ssl, sd);
        err = SSL_accept(ssl);
        if (err > 0) {
            CHK_SSL(err);
            return new SSLClient(clientFd, ssl);
        }
        return new Client(clientFd);
    }

    SSL_CTX* ctx;

    std::vector<pollfd> polls;
    std::vector<Client*> sslClient;

    //! server fd
    int sd;

public:
    BothServer() {
        ctx = initSSL();

        sockaddr_in inadr{};
        inadr.sin_family = AF_INET;
        inadr.sin_port = htons(12345);
        inadr.sin_addr.s_addr = INADDR_ANY;

        sd = socket(AF_INET, SOCK_STREAM, 0);

        int opt = 1;
        TRY("reuse_port", setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)));
        TRY("bind", bind(sd, (sockaddr*) &inadr, sizeof(inadr)));
        TRY("listen", listen(sd, 100));

        polls.push_back({sd, POLLIN});
        sslClient.push_back(new Client(sd));
    }

    virtual ~BothServer() {
        close(sd);
        SSL_CTX_free(ctx);
    }


    void run() {
        while (true) {
            int pollRes = poll(polls.data(), polls.size(), 10000);
            if (pollRes < 0) TRY("poll", pollRes);
            if (pollRes == 0) {
                continue;
            }

            int i = -1;
            for (auto &poll: polls) {
                i++;
                if (poll.revents & POLLHUP) {
                    poll.fd = -1;
                    continue;
                }

                if (!(poll.revents & POLLIN)) {
                    continue;
                }

                if (poll.fd == sd) { // server socket
                    printf("new client\n");
                    sockaddr_in peeradr{};
                    socklen_t len_adr = sizeof(peeradr);
                    int newsock = accept(sd, (sockaddr*) &peeradr, &len_adr);
                    sslClient.push_back(acceptSSLClient(newsock, ctx));
                    polls.push_back({newsock, POLLIN | POLLHUP | POLLNVAL});

                } else {
                    printf("new message\n");
                    char message[255];
                    int size = sslClient[i]->mread(message, sizeof(message));
                    CHK_SSL(size);

                    if (size == 0) {
                        poll.fd = -1;
                        continue;
                    }

                    std::string msg(message);
                    if (msg.find("!close") != std::string::npos) {
                        poll.fd = -1;
                        continue;
                    }

                    int i2 = 0;
                    for (auto broadcastPoll: polls) {
                        if (!(broadcastPoll.fd == sd || broadcastPoll.fd == -1)) { // not the server socket
                            int writeRes = sslClient[i2]->mwrite(message, size);
                            CHK_SSL(writeRes)
                            if (writeRes < 0) {
                                broadcastPoll.fd = -1;
                            }
                        }
                        i2++;
                    }
                }
            }

            auto pollIterator = polls.begin();
            auto sslClientIterator = sslClient.begin();
            for (int i = 0; i < polls.size(); ++i) {
                if (pollIterator->fd == -1) {
                    delete (*sslClientIterator);
                    sslClientIterator = sslClient.erase(sslClientIterator);
                    pollIterator = polls.erase(pollIterator);
                } else {
                    pollIterator++;
                    sslClientIterator++;
                }
            }
        }
    }

};

int main() {
    BothServer both;
    both.run();

    return 0;
}

/**
sodae@sodae-v3 ~ $ telnet localhost 12345
Trying ::1...
Connection failed: Spojení odmítnuto
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
jaja

nic
ananas
ananas
trorl


---------------------

 sodae@sodae-v3 ~ $ openssl s_client -connect localhost:12345
CONNECTED(00000003)
depth=0 C = AU, ST = Some-State, O = Internet Widgits Pty Ltd, CN = localhost
verify error:num=18:self signed certificate
verify return:1
depth=0 C = AU, ST = Some-State, O = Internet Widgits Pty Ltd, CN = localhost
verify return:1
---
Certificate chain
 0 s:/C=AU/ST=Some-State/O=Internet Widgits Pty Ltd/CN=localhost
   i:/C=AU/ST=Some-State/O=Internet Widgits Pty Ltd/CN=localhost
---
Server certificate
-----BEGIN CERTIFICATE-----
MIIDiDCCAnCgAwIBAgIJAM8cHD3nePtGMA0GCSqGSIb3DQEBCwUAMFkxCzAJBgNV
BAYTAkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMSEwHwYDVQQKDBhJbnRlcm5ldCBX
aWRnaXRzIFB0eSBMdGQxEjAQBgNVBAMMCWxvY2FsaG9zdDAeFw0xNzExMjgyMjE0
MTRaFw0xODExMjgyMjE0MTRaMFkxCzAJBgNVBAYTAkFVMRMwEQYDVQQIDApTb21l
LVN0YXRlMSEwHwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQxEjAQBgNV
BAMMCWxvY2FsaG9zdDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAL67
1d6Ejw1z5DT7OkBPFgKqwV2pj4RTdaGV/w44xfSoI5WZZCk6Gz2GYH6Qbx6bVsBz
FfQtp2b6zfqMcT4lPRvBivPaa4dQkjMm9uauZW9MtQx9BHz2+/bnSeVXGMZVM3D5
OqqIzoeg7fTLazewcIprlbubN8os5rAZECa8wElBIdfHxDJH4i2dZ7EbhcZvHO9n
CmrXZ/VZc1y9bhWFWRi5IXQOJGPXpKHYLdlWqw7pMhTW/W4CcaZfMO/ZYw1bpG61
xIzckKCG8/uZkZWAW8/1wkxz8Tk6DCyLagczRQ97yyUy2S9tWzJ57Pl5zkBPcWav
1LWini4Au/TnmT1X/SMCAwEAAaNTMFEwHQYDVR0OBBYEFLFpFGyTYCEkhQm/NIPT
U6pojxSIMB8GA1UdIwQYMBaAFLFpFGyTYCEkhQm/NIPTU6pojxSIMA8GA1UdEwEB
/wQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAFgzDDrXmAu0YGJ73/hWrSzSlP6z
/Oy+A7XVdfHtRF6IvP4XgyD1AJi9GEZhtmXvdNytItf1uIAjQeS0KelHaxLm3eIo
EhPhi8hAwnkrc+1ZxxtGZB8uiwg2FnmpkVRQXMjN95HPHwOZoMIELKrqFh/eFtOY
3aeUP3ljdj7JGPnO/22pTSIixfCnKIPIh2+kLI+121vP/4I3fABPTABdDuA+lXr2
aJ0dwedm6zPtgGEg0Z2bcp+gWxXkSBFZdJwXEfTn3lLvXvM12pssU4y/xHx57nEG
TfdOfvMZ+DF2kYx9Xy3BO6PBAB0AM4lnEZU5jofu7ySPRBpTZK9obfhVtdU=
-----END CERTIFICATE-----
subject=/C=AU/ST=Some-State/O=Internet Widgits Pty Ltd/CN=localhost
issuer=/C=AU/ST=Some-State/O=Internet Widgits Pty Ltd/CN=localhost
---
No client certificate CA names sent
Peer signing digest: SHA512
Server Temp Key: X25519, 253 bits
---
SSL handshake has read 1533 bytes and written 269 bytes
Verification error: self signed certificate
---
New, TLSv1.2, Cipher is ECDHE-RSA-AES256-GCM-SHA384
Server public key is 2048 bit
Secure Renegotiation IS supported
Compression: NONE
Expansion: NONE
No ALPN negotiated
SSL-Session:
    Protocol  : TLSv1.2
    Cipher    : ECDHE-RSA-AES256-GCM-SHA384
    Session-ID: 095627013CC2AD97542F18A071C82177D7910CB4BA5A3D694A1F8FA90F16F0C7
    Session-ID-ctx:
    Master-Key: A693461359F0DF918C6FF3FE70B3EB2BFB7AE1F414923C24EB1656FECA2A716D05DBB38948743E10FE8E26A0D49E70B4
    PSK identity: None
    PSK identity hint: None
    SRP username: None
    TLS session ticket lifetime hint: 7200 (seconds)
    TLS session ticket:
    0000 - 6e f3 16 c4 5e f3 79 74-6a c9 7b f7 9e 08 63 d6   n...^.ytj.{...c.
    0010 - d2 c7 bd 2b 75 33 08 cb-33 b7 3d f7 c7 52 1f 66   ...+u3..3.=..R.f
    0020 - 93 be 84 b1 f3 3d 46 d4-6a 09 8b 0c a6 6c 5a 68   .....=F.j....lZh
    0030 - b2 09 ed ba bf da 48 1b-88 e2 4c e4 86 54 e3 f8   ......H...L..T..
    0040 - 43 86 bc 82 0c 81 db e7-20 89 c2 84 98 11 81 af   C....... .......
    0050 - e4 cd df c3 23 9d f1 38-92 be f0 83 77 0c fd 62   ....#..8....w..b
    0060 - b6 4f a9 ae 4c b4 81 c8-6d 47 21 1f b4 c3 91 2a   .O..L...mG!....*
    0070 - ee c5 dc 94 46 30 53 44-fa 47 3e 56 25 1d cf 2f   ....F0SD.G>V%../
    0080 - a0 3f 83 df 4f a8 9b 17-6d 09 ac 7d 13 94 7f 40   .?..O...m..}...@
    0090 - 01 3a f0 39 4a a9 86 28-cb 94 e5 8c 98 0b a4 ed   .:.9J..(........

    Start Time: 1513691317
    Timeout   : 7200 (sec)
    Verify return code: 18 (self signed certificate)
    Extended master secret: yes
---
ahoj
ahoj

nic
nic
ananas
trorl
trorl



**/