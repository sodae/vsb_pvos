#include <iostream>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <poll.h>
#include <vector>
#include <algorithm>


#define TRY(x, y) { if (y < 0) { printf("code %d\n", y); perror(x); exit(254); } }


int main_poll() {
    std::vector<pollfd> polls;

    sockaddr_in inadr{};
    inadr.sin_family = AF_INET;
    inadr.sin_port = htons(12345);
    inadr.sin_addr.s_addr = INADDR_ANY;

    int sd = socket(AF_INET, SOCK_STREAM, 0);
    TRY("bind", bind(sd, (sockaddr *) &inadr, sizeof(inadr)));
    TRY("listen", listen(sd, 100));

    polls.push_back({sd, POLLIN});

    while (true) {
        TRY("poll", poll(polls.data(), polls.size(), 10000))
        printf("new round\n");
        for (auto poll: polls) {
            if (!(poll.revents & POLLIN))
                continue;

            if (poll.fd == sd) { // server socket
                printf("new client\n");
                sockaddr_in peeradr{};
                socklen_t len_adr = sizeof(peeradr);
                int newsock = accept(sd, (sockaddr *) &peeradr, &len_adr);

                polls.push_back({newsock, POLLIN});
            } else {
                printf("new message\n");
                char message[255];
                int size = read(poll.fd, message, sizeof(message));
                TRY("read", size);

                std::string msg(message);
                if (msg.find("!close") != std::string::npos) {
                    close(poll.fd);
                    poll.fd = -1;
                }

                for (auto broadcastPoll: polls) {
                    if (broadcastPoll.fd == sd) { // server socket
                        continue;
                    }
                    int writeRes = write(broadcastPoll.fd, message, size * sizeof(char));
                    //TRY("write", );
                    if (writeRes < 0) {
                        broadcastPoll.fd = -1;
                    }
                }
            }
        }

        polls.erase(
                std::remove_if(polls.begin(), polls.end(), [&](pollfd const &poll) {
                    return poll.fd == -1;
                }),
                polls.end()
        );
    }


    //close(newsock);
    close(sd);
    return 0;
}


int main() {
    std::vector<fd_set> polls;
    std::vector<int> fds;

    sockaddr_in inadr{};
    inadr.sin_family = AF_INET;
    inadr.sin_port = htons(12345);
    inadr.sin_addr.s_addr = INADDR_ANY;

    int sd = socket(AF_INET, SOCK_STREAM, 0);
    TRY("bind", bind(sd, (sockaddr *) &inadr, sizeof(inadr)));
    TRY("listen", listen(sd, 100));

    fd_set socketFdSet;
    polls.push_back(socketFdSet);
    fds.push_back(sd);

    while (true) {
        FD_ZERO(polls.data());
        int maxFd = 0;
        for (int i = 0; i < fds.size(); i++) {
            FD_SET(fds[i], polls.data());
            maxFd = std::max(maxFd, fds[i]);
        }

        timeval timeout = {1};
        int sel = select(maxFd + 1, polls.data(), NULL, NULL, &timeout);
        TRY("select", sel);
        printf("new round\n");

        if (FD_ISSET(sd, polls.data())) { // server socket
            printf("new client\n");
            sockaddr_in peeradr{};
            socklen_t len_adr = sizeof(peeradr);
            int newsock = accept(sd, (sockaddr *) &peeradr, &len_adr);

            fd_set newSocketFdSet{};
            FD_ZERO(&newSocketFdSet);
            FD_SET(newsock, &newSocketFdSet);
            polls.push_back(newSocketFdSet);
            fds.push_back(newsock);
        }
        for (int i = 1; i < fds.size(); i++) {
            if (FD_ISSET(fds[i], polls.data())) {
                printf("new message\n");
                char message[255];
                int size = read(fds[i], message, sizeof(message));
                TRY("read", size);

                std::string msg(message);
                if (msg.find("!close") != std::string::npos) {
                    close(fds[i]);
                    fds[i] = -1;
                }

                for (int j = 1; j < fds.size(); j++) { // skip server
                    int writeRes = write(fds[j], message, size * sizeof(char));
                    //TRY("write", );
                    if (writeRes < 0) {
                        fds[i] = -1;
                    }
                }
            }
        }
        for (int i = 0; i < fds.size(); i++) {
            if (fds[i] == -1) {
                fds.erase(fds.begin() + i);
                polls.erase(polls.begin() + i);
            }
        }

    }


    //close(newsock);
    close(sd);
    return 0;
}