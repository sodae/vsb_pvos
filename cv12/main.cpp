#include <iostream>
#include <unistd.h>
#include <poll.h>
#include <vector>
#include <algorithm>
#include <csignal>
#include <cstring>
#include <termios.h>
#include <sys/time.h>
#include <fcntl.h>

#define TRY(x, y) { if (y < 0) { printf("code %d\n", y); perror(x); exit(254); } }

/**
 * @return done
 */
bool tryRead(int __fd, char* buf, int maxBuf, int &loadedLen) {
    char c = '\0';
    for (; loadedLen < maxBuf; loadedLen++) {
        int readSize = read(__fd, &c, 1);
        if (readSize <= 0) {
            return false;
        }
        buf[loadedLen] = c;
        if (c == '\n') {
            loadedLen++;
            break;
        };
    }
    return true;
}

int read_line_pool(int fd, char* buf, int len, int timeoutSeconds) {
    struct timeval t_end, t_now, t_diff;
    gettimeofday(&t_end, NULL);
    t_end.tv_sec += timeoutSeconds;

    int loadedLen = 0;

    while (true) {
        gettimeofday(&t_now, NULL);
        timersub(&t_end, &t_now, &t_diff);

        struct pollfd pfd[1];
        pfd[0].fd = fd;
        pfd[0].events = POLLIN;
        int sel = poll(pfd, 1, t_diff.tv_sec * 1000);

        if (sel > 0 && pfd[0].revents & POLLIN) {
            if (tryRead(fd, buf, len, loadedLen)) {
                break;
            }
        } else {
            break;
        }
    }
    return loadedLen;
}

/** Kdyz neco prijde z jedne pipe, vleje to do druhe pipe a naopak */
int transport(
        int client[2], // 0 = read fd, 1 = write fd
        int second[2] // 0 = read fd, 1 = write fd
) {
    std::vector<pollfd> polls;

    polls.push_back({client[0], POLLIN});
    polls.push_back({second[0], POLLIN});

    while (true) {
        int pollRes = poll(polls.data(), polls.size(), 10000);
        if (pollRes < 0) return pollRes;

        for (auto &poll: polls) {
            if (!(poll.revents & POLLIN)) {
                continue;
            }

            char msg[255] = "";
            int len = read_line_pool(poll.fd, msg, 255, 10);
            if (len == 0) {
                printf("%d (%d)\n", poll.fd, len);
                return -1;
            }
            printf("%d (%d): %s\n", poll.fd, len, msg);
            if (poll.fd == second[0]) {
                //write(client[1], msg, len);
            } else {
                write(second[1], msg, len);
            }
        }
    }

}

// otevre kanal pro serial
int open_serial(char* device, int flag) {
    int fd = open(device, flag);

    struct termios cfg;
    cfmakeraw(&cfg);
    cfsetspeed(&cfg, B115200);
    tcsetattr(fd, TCSAFLUSH, &cfg);

    return fd;
}

int main() {
    int client[2] = {
            STDIN_FILENO,
            STDOUT_FILENO
    };
/*
    char* pipeFile = "/home/sodae/pipe";
    int server[2] = {
            open(pipeFile, O_RDONLY | O_NONBLOCK),
            open(pipeFile, O_WRONLY | O_NONBLOCK)
    };
*/
    char* device = "/dev/USBtty0";
    int server[2] = {
            open_serial(device, O_RDONLY), // nebo jeden FD s O_RDWR
            open_serial(device, O_WRONLY)
    };

    transport(client, server);
    return 0;
}