#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <sys/param.h>
#include <aio.h>
#include <pthread.h>
#include <ctime>
#include <iostream>
#include <sys/mman.h>

/**
 * 1. Zmerte rychlost preneseni 100GB data mezi
 * procesy pomoci anonymni roury, pojmenovane roury a
  pomoci sdilene pameti, kdy sdilena pamet bude
  cca 1-10MB a 2 roury budou synchronizovat data.
  Prenaset se budou inty, obe strany udelaji soucet do long.

2. Overte, ze data v souboru je mozne pouzit stejne pres read/write i pres mmap.
  struct data { float cisla[ 10 ], suma };

  Rodic vytvari postupne potomky, ne soucasne vice. Potomek nahodne precte soubor pres read,
  nebo pres mmap, zkontroluje soucet, zmeni jedno cislo, upravi soucet a ulozi data.

 */

const long size = 100l * 1024l * 1024 * 1024l; // 100GB
const long maxPipeBufferSize = 2048; // by internet
const long bufferSize = maxPipeBufferSize / sizeof(int);
const long iterations = size / maxPipeBufferSize;


void task_1_writing(int pipe) {
    int *buff = new int[bufferSize];
    long sum = 0;
    for (long i = 0; i < iterations; i++) {
        for (long p = 0; p < bufferSize; p++) {
            buff[p] = p % 100000;
            sum += buff[p];
        }
        int a = write(pipe, buff, maxPipeBufferSize);
        if (a < 0) {
            perror("child error");
            exit(0);
        }
    }
    std::cout << "child has died" << std::endl;
    std::cout << "child sum:" << sum << std::endl;

    delete buff;
}

void task_1_reading(int pipe) {
    int *buff = new int[bufferSize];
    long sum = 0;
    std::clock_t start = std::clock();
    for (long i = 0; i < iterations; i++) {
        int a = read(pipe, buff, maxPipeBufferSize);
        if (a < 0) {
            perror("parent error");
            exit(0);
        }
        for (long p = 0; p < bufferSize; p++) {
            sum = sum + buff[p];
        }
    }
    double duration = (std::clock() - start) / (double) CLOCKS_PER_SEC;
    std::cout << "Time:" << duration << "s" << std::endl;
    std::cout << "Speed (MB): " << ((size / duration) / (1024 * 1024)) << std::endl;
    std::cout << "parent sum:" << sum << std::endl;
    wait(NULL);
}

void task_1_anonymous() {
    int pipes[2];
    pipe(pipes);
    int isParent = fork();
    if (!isParent) {
        task_1_writing(pipes[1]);
    } else {
        task_1_reading(pipes[0]);
    }

    close(pipes[0]);
    close(pipes[1]);
}

void task_1_named() {
    std::string file = "/tmp/trubka";
    unlink(file.c_str());
    if (mkfifo(file.c_str(), 0660) < 0) perror("mkfifo");
    int isParent = fork();

    if (!isParent) {
        int pipee = open(file.c_str(), O_WRONLY);
        if (pipee < 0) perror("child open");
        task_1_writing(pipee);
    } else {
        int pipee = open(file.c_str(), O_RDONLY);
        if (pipee < 0) perror("parent open");
        task_1_reading(pipee);
    }
}

template<typename Functor>
void task_1_mmap_worker(int signalRead, int signalWrite, bool firstAndChild, long iterations, Functor worker) {
    int buf = 0;
    long sum = 0;
    std::clock_t start = std::clock();
    for (long i = 0; i < iterations; i++) {
        if (!firstAndChild || i > 0) read(signalRead, &buf, sizeof(buf));
        worker(&sum);
        write(signalWrite, &buf, sizeof(buf));
    }
    if (firstAndChild) {
        std::cout << "child sum:" << sum << std::endl;
    } else {
        double duration = (std::clock() - start) / (double) CLOCKS_PER_SEC;
        std::cout << "Time:" << duration << "s" << std::endl;
        std::cout << "Speed (MB): " << ((size / duration) / (1024 * 1024)) << std::endl;
        std::cout << "parent sum:" << sum << std::endl;
    }

}

void task_1_mmap() {
    long blockSize = 10 * 1024 * 1024;
    int *data = (int *) mmap(NULL, blockSize, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    long units = blockSize / sizeof(int);
    long iterations = size / blockSize;

    int readerPipe[2];
    int writerPipe[2];
    pipe(readerPipe);
    pipe(writerPipe);

    int isParent = fork();
    if (isParent) {
        task_1_mmap_worker(readerPipe[0], writerPipe[1], false, iterations, [&](long *sum) {
            for (long p = 0; p < units; p++) {
                (*sum) += data[p];
            }
        });
    } else {
        task_1_mmap_worker(writerPipe[0], readerPipe[1], true, iterations, [&](long *sum) {
            for (long p = 0; p < units; p++) {
                data[p] = p % 100000;
                (*sum) += data[p];
            }
        });
    }
}

struct data {
    float cisla[10], suma;
};

void task_2_check(data *data) {
    float sum = 0;
    for (int i = 0; i < 10; i++) {
        sum += data->cisla[i];
    }
    if (sum != data->suma) {
        printf("Invalid\n");
    } else {
        printf("OK!\n");
    }
}

void task_2_update(data *data) {
    float sum = 0;
    int i = rand() % 10;
    data->cisla[i] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
    for (int i = 0; i < 10; i++) {
        sum += data->cisla[i];
    }
    data->suma = sum;
}

void task_2() {
    data block = {0};
    std::string path = "data.dat";
    int fd = open(path.c_str(), O_CREAT | O_WRONLY);
    write(fd, &block, sizeof(data));
    close(fd);

    for (int i = 0; i < 100; i++) {
        int isParent = fork();
        std::srand(getpid());
        if (!isParent) {
            int openedFile = shm_open(path.c_str(), O_CREAT | O_RDWR, 0660);
            ftruncate(openedFile, sizeof(data));

            if (std::rand() % 2 == 0) {
                printf("mmap\n");
                data *ptrdata = (data *) mmap(NULL, sizeof(data), PROT_READ | PROT_WRITE, MAP_SHARED, openedFile, 0);
                task_2_check(ptrdata);
                task_2_update(ptrdata);
                task_2_check(ptrdata);
                msync(ptrdata, sizeof(data), MS_SYNC);
            } else {
                printf("file\n");
                data *ptrdata;
                read(openedFile, ptrdata, sizeof(data));
                task_2_check(ptrdata);
                task_2_update(ptrdata);
                task_2_check(ptrdata);
                write(openedFile, ptrdata, sizeof(data));
            }
            close(openedFile);
            return;
        }
        usleep(100);
        waitpid(isParent, NULL, 0);
    }
}

int main() {
    /**
child has died
Time:141.444s
child sum:6858525900800
Speed (MB): 723.96
parent sum:6858525900800
     */
    task_1_anonymous();


    /**
child has died
child sum:6858525900800
Time:157.119s
Speed (MB): 651.737
parent sum:6858525900800
     */
    task_1_named();

/**
child sum:1333540107059200
Time:84.6796s
Speed (MB): 1209.26
parent sum:1333540107059200
 */
    task_1_mmap();


/* funguje krásně */
    task_2();

}

