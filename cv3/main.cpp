#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/select.h>
#include <sys/param.h>
#include <poll.h>
#include <sys/time.h>

/*
Nastaveni terminalu na canon/no-canon mode
  stty -F /dev/tty icanon
  stty -F /dev/tty -icanon

Implementujte funkci read_line s timeoutem

  int read_line( char *buf, int len, int timeout_ms );
  int read_line( char *buf, int *len, int timeout_ms );

1. pomoci select
2. pomoci poll (gettimeofday, timeradd, timersub, timercmp)
3. O_NONBLOCK (usleep( 10000);)

 */

/**
 * @return done
 */
bool tryRead(int __fd, char *buf, int maxBuf, int &loadedLen) {
    char c = '\0';
    for (; loadedLen < maxBuf; loadedLen++) {
        int readSize = read(__fd, &c, 1);
        if (readSize <= 0) {
            return false;
        }
        buf[loadedLen] = c;
        if (c == '\n') {
            loadedLen++;
            break;
        };
    }
    return true;
}

int read_line_select(char *buf, int len, int timeoutSeconds) {
    timeval timeout = {0, 0};
    timeout.tv_sec = timeoutSeconds;

    int loadedLen = 0;

    while (true) {
        fd_set myDescriptor;
        FD_ZERO(&myDescriptor);
        FD_SET(STDIN_FILENO, &myDescriptor);

        int sel = select(1, &myDescriptor, NULL, NULL, &timeout);
        if (sel == -1) {
            perror("read_line_select");
            return 0;
        }
        if (sel == 0) {
            return 0;
        }
        if (tryRead(STDIN_FILENO, buf, len, loadedLen)) {
            break;
        }
    }

    return loadedLen;
}

int read_line_pool(char *buf, int len, int timeoutSeconds) {
    struct timeval t_end, t_now, t_diff;
    gettimeofday(&t_end, NULL);
    t_end.tv_sec += timeoutSeconds;

    int loadedLen = 0;

    while (true) {
        gettimeofday(&t_now, NULL);
        timersub(&t_end, &t_now, &t_diff);

        struct pollfd pfd[1];
        pfd[0].fd = STDIN_FILENO;
        pfd[0].events = POLL_IN;
        int sel = poll(pfd, 1, t_diff.tv_sec * 1000);

        if (sel > 0 && pfd[0].revents & POLLIN) {
            if (tryRead(STDIN_FILENO, buf, len, loadedLen)) {
                break;
            }
        } else {
            break;
        }
    }
    return loadedLen;
}

int read_line_block(char *buf, int len, int timeoutSeconds) {
    struct timeval t_end, t_now, t_diff;
    gettimeofday(&t_end, NULL);
    t_end.tv_sec += timeoutSeconds;

    int loadedLen = 0;

    fcntl(STDIN_FILENO, F_SETFL, fcntl(STDIN_FILENO, F_GETFL) | O_NONBLOCK);
    while (true) {

        gettimeofday(&t_now, NULL);
        timersub(&t_end, &t_now, &t_diff);
        if (t_diff.tv_sec <= 0) {
            break;
        }

        if (tryRead(STDIN_FILENO, buf, len, loadedLen)) {
            break;
        }

        usleep(1000);
    }
    return loadedLen;
}


int main() {
    char in[255] = "";
    std::cout << "write:";
    std::cout.flush();
    //std::cout << read_line_select(in, 255, 10);
    //std::cout << " is size, input is:" << std::string(in) << std::endl;
    in[0] = '\0';
    std::cout << read_line_block(in, 255, 10);
    //std::cout << " is size, input is:" << std::string(in) << std::endl;
    in[0] = '\0';
    //std::cout << read_line_pool(in, 255, 10);
    std::cout << " is size, input is:" << std::string(in) << std::endl;

    return 0;
}