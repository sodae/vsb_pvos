#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <vector>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/select.h>
#include <sys/param.h>
#include <poll.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <algorithm>
#include <csignal>
#include <arpa/inet.h>
#include <cstring>
#include <termios.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <unordered_map>
#include <mqueue.h>
#include <sys/msg.h>
#include <semaphore.h>

#include <openssl/rsa.h>       /* SSLeay stuff */
#include <openssl/x509.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#define TRY(x, y) { if (y < 0) { printf("code %d\n", y); perror(x); exit(254); } }
#define CHK_NULL(x) if ((x)==NULL) exit (1)
#define CHK_SSL(err) if ((err)==-1) { ERR_print_errors_fp(stderr); exit(2); }

#define HOME "./crts/"
#define CERTF  HOME "myserver.crt"
#define KEYF  HOME  "mypriv.pem"

/**
 * @return done
 */
bool tryRead(int __fd, char* buf, int maxBuf, int &loadedLen) {
    char c = '\0';
    for (; loadedLen < maxBuf; loadedLen++) {
        int readSize = read(__fd, &c, 1);
        if (readSize <= 0) {
            return false;
        }
        buf[loadedLen] = c;
        if (c == '\n') {
            loadedLen++;
            break;
        };
    }
    return true;
}

struct fdrl {
    int fd;
    char str[1000]; // doufam, ze nevadi, ze to udelam staticky nyní
    int size = 0;
    bool done = false; // full line
    int maxSize = 1000;
};

bool read_line_poll(std::vector<fdrl> &fdrls, int timeoutSeconds) {
    struct timeval t_end, t_now, t_diff;
    gettimeofday(&t_end, NULL);
    t_end.tv_sec += timeoutSeconds;

    std::vector<pollfd> fds;
    for (auto &fdrl_: fdrls) {
        fdrl_.done = false;
        fdrl_.str[0] = '\n';
        fdrl_.size = 0;
        fds.push_back({fdrl_.fd, POLLIN});
    }

    while (t_diff.tv_sec > 0) {
        gettimeofday(&t_now, NULL);
        timersub(&t_end, &t_now, &t_diff);

        int sel = poll(fds.data(), fds.size(), t_diff.tv_sec * 1000);
        TRY("poll", sel);

        int y = -1;
        for (auto fd: fds) {
            y++;
            if (!fdrls[y].done && fd.revents & POLLIN) {
                fdrls[y].done = tryRead(fd.fd, fdrls[y].str, fdrls[y].maxSize, fdrls[y].size);
            }
        }

        bool done = true;
        for (auto fdrl_: fdrls) {
            done = done && fdrl_.done;
        }
        if (done) {
            return true;
        }
    }

    bool done = true;
    for (auto fdrl_: fdrls) {
        done = done && fdrl_.done;
    }
    return done;
}

int main() {
    sockaddr_in inadr{};
    inadr.sin_family = AF_INET;
    inadr.sin_port = htons(12345);
    inadr.sin_addr.s_addr = INADDR_ANY;

    int serverSocketFd = socket(AF_INET, SOCK_STREAM, 0);

    int opt = 1;
    TRY("reuse_port", setsockopt(serverSocketFd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)));
    TRY("bind", bind(serverSocketFd, (sockaddr*) &inadr, sizeof(inadr)));
    TRY("listen", listen(serverSocketFd, 100));

    sockaddr_in peeradr{};
    socklen_t len_adr = sizeof(peeradr);
    int newsock = accept(serverSocketFd, (sockaddr*) &peeradr, &len_adr);
    printf("new client\n");

    std::vector<fdrl> fdrls;
    fdrls.push_back({
            STDIN_FILENO
    });
    fdrls.push_back({
            newsock
    });

    bool allDone = read_line_poll(fdrls, 10);
    printf("alldone: %d\n", allDone);

    for (auto fd: fdrls) {
        printf("fd=%d (size=%d) (done=%d): \"%s\"\n", fd.fd, fd.size, fd.done, fd.str);
    }
}

/** (oba neco napisu)
sodae@sodae-v3 ~/Dokumenty/vsb/povs/priprava/cmake-build-debug (master) $   stty -F /dev/tty -icanon
sodae@sodae-v3 ~/Dokumenty/vsb/povs/priprava/cmake-build-debug (master) $ ./priprava
new client
malicky
alldone: 1
fd=0 (size=8) (done=1): "malicky
"
fd=4 (size=7) (done=1): "anans
"

 ------(ze socketu jen neco napise)
sodae@sodae-v3 ~/Dokumenty/vsb/povs/priprava/cmake-build-debug (master) $   stty -F /dev/tty -icanon
sodae@sodae-v3 ~/Dokumenty/vsb/povs/priprava/cmake-build-debug (master) $ ./priprava
new client
alldone: 0
fd=0 (size=0) (done=0): ""
fd=4 (size=8) (done=1): "ananas
"


 ------(ze konzole jen neco napise)
sodae@sodae-v3 ~/Dokumenty/vsb/povs/priprava/cmake-build-debug (master) $   stty -F /dev/tty -icanon
sodae@sodae-v3 ~/Dokumenty/vsb/povs/priprava/cmake-build-debug (master) $ ./priprava
new client
malyalesikovny
alldone: 0
fd=0 (size=15) (done=1): "malyalesikovny
"
fd=4 (size=0) (done=0): ""


 -------(nikdo nic nenapise)
sodae@sodae-v3 ~/Dokumenty/vsb/povs/priprava/cmake-build-debug (master) $   stty -F /dev/tty -icanon
sodae@sodae-v3 ~/Dokumenty/vsb/povs/priprava/cmake-build-debug (master) $ ./priprava
new client
alldone: 0
fd=0 (size=0) (done=0): ""
fd=4 (size=0) (done=0): ""
sodae@sodae-v3 ~/Dokumenty/vsb/povs/priprava/cmake-build-debug (master) $

 */