#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/param.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <unordered_map>

/*
Napiste aplikaci simulujici plneni prepravky pro MAX vyrobku.
Rodic bude mit minimalne 10 potomku.
Potomci plni prepravku, ta je ve sdilene pameti: struct { int pocet, vyrobky[ MAX ]; }
Kazdy potomek vzdy precte pocet, zapise svuj pid mezi vyrobky[pocet++]
Pokud potomek vlozi na posledni volne misto, musi potomci pockat na vymenu prepravky.

1. Implementace pomoci semaforu System V, rodic snizuje semafor prepracky o MAX.
  Rodic pri vyprazdneni prepravky dela statistiku, kolik vyrobku dodal ktery potomek
  (jeho pid ve sdilene pameti). Vhodne pouzit unordered_map (hashovaci tabulka), c++11.

2. Implementace pomoci semaforu System V, rodic vyuzije vlastnosti wait-for-zero.

3. Implementace pomoci Posix semaforu.
 */

#define GOODS_PER_WORKER 100000

#define WORKERS 10

#define MAX_VYROBKU 1000

struct prepravka {
    int pocet = 0;
    int vyrobky[MAX_VYROBKU];
};

int WORKER_NUM_SEM = 0;
int CONSUMER_NUM_SEM = 1;

void releaseUp(int semNum, int howMany = 1) {
    sembuf buf = {0, howMany, 0};
    int a = semop(semNum, &buf, 1);
    if (a < 0) {
        printf("ret(%d) semNum(%d), howmany(%d)", a, semNum, howMany);
        perror("releaseUp");
        exit(1);
    }
}

void takeDown(int semNum, int howMany = 1) {
    sembuf buf = {0, -howMany, 0};
    int a = semop(semNum, &buf, 1);
    if (a < 0) {
        printf("ret(%d) semNum(%d), howmany(%d)", a, semNum, howMany);
        perror("takeDown");
        exit(1);
    }
}

void waitZero(int semNum) {
    sembuf buf = {0, 0, 0};
    int a = semop(semNum, &buf, 1);
    if (a < 0) {
        printf("ret(%d) semNum(%d)", a, semNum);

        perror("waitZero");
        exit(1);
    }
}

void task1_worker(int pid, prepravka *crate) {
    for (int i = 0; i < GOODS_PER_WORKER; i++) {
        takeDown(WORKER_NUM_SEM);
        int count = crate->pocet;
        crate->vyrobky[count++] = pid;
        crate->pocet = count;
        std::cout << "." << std::endl;
        releaseUp(CONSUMER_NUM_SEM);
        if (count < MAX_VYROBKU) {
            releaseUp(WORKER_NUM_SEM);
        }
    }
}

void task1_consumer(prepravka *crate) {
    int maxCratesPerDay = (GOODS_PER_WORKER * WORKERS) / MAX_VYROBKU;

    for (int cratesCount = 0; cratesCount < maxCratesPerDay; cratesCount++) {
        takeDown(CONSUMER_NUM_SEM, MAX_VYROBKU);

        std::unordered_map<int, int> statistics;
        for (int i = 0; i < MAX_VYROBKU; i++) {
            int pid = crate->vyrobky[i];
            statistics[pid]++;
        }

        for (auto row: statistics) {
            std::cout << row.first << ": " << row.second << std::endl;
            if (row.first == 0) exit(-2);
        }

        crate->pocet = 0;
        memset(crate->vyrobky, 0, sizeof(crate->vyrobky));

        std::cout << "----------------------" << std::endl;

        releaseUp(WORKER_NUM_SEM);
    }
}

void task1() {
    CONSUMER_NUM_SEM = semget(0xDEADBEEF, 1, IPC_CREAT | 0600);
    semctl(CONSUMER_NUM_SEM, 0, SETVAL, 0);
    if (CONSUMER_NUM_SEM < 0) {
        perror("cannot create semaphore");
        exit(1);
    }

    WORKER_NUM_SEM = semget(0x01010101, 1, IPC_CREAT | 0600);
    semctl(WORKER_NUM_SEM, 0, SETVAL, 1);
    if (WORKER_NUM_SEM < 0) {
        perror("cannot create semaphore 2");
        exit(1);
    }

    auto *data = (prepravka *) mmap(
            nullptr,
            sizeof(prepravka),
            PROT_READ | PROT_WRITE,
            MAP_SHARED | MAP_ANONYMOUS,
            -1,
            0
    );

    int childId = 1;
    for (int i = 0; i < WORKERS && childId; i++) {
        childId = fork();
    }

    if (childId) {
        task1_consumer(data);
        wait(NULL);

        semctl(WORKER_NUM_SEM, 0, IPC_RMID);
        semctl(CONSUMER_NUM_SEM, 0, IPC_RMID);
    } else {
        task1_worker(getpid(), data);
    }
}


void task2_worker(int pid, prepravka *crate) {
    for (int i = 0; i < GOODS_PER_WORKER; i++) {
        takeDown(WORKER_NUM_SEM);
        int count = crate->pocet;
        crate->vyrobky[count++] = pid;
        crate->pocet = count;
        takeDown(CONSUMER_NUM_SEM);
        if (count < MAX_VYROBKU) {
            releaseUp(WORKER_NUM_SEM);
        }
    }
}

void task2_consumer(prepravka *crate) {
    int maxCratesPerDay = (GOODS_PER_WORKER * WORKERS) / MAX_VYROBKU;

    for (int cratesCount = 0; cratesCount < maxCratesPerDay; cratesCount++) {
        waitZero(CONSUMER_NUM_SEM);
        semctl(CONSUMER_NUM_SEM, 0, SETVAL, MAX_VYROBKU);

        std::unordered_map<int, int> statistics;
        for (int i = 0; i < MAX_VYROBKU; i++) {
            int pid = crate->vyrobky[i];
            statistics[pid]++;
        }

        for (auto row: statistics) {
            std::cout << row.first << ": " << row.second << std::endl;
            if (row.first == 0) exit(2);
        }

        crate->pocet = 0;
        memset(crate->vyrobky, 0, sizeof(crate->vyrobky));

        std::cout << "----------------------" << std::endl;

        releaseUp(WORKER_NUM_SEM);
    }
}

void task2() {
    CONSUMER_NUM_SEM = semget(0xDEADBEEF, 1, IPC_CREAT | 0600);
    if (CONSUMER_NUM_SEM < 0) {
        perror("cannot create semaphore");
        exit(1);
    }
    semctl(CONSUMER_NUM_SEM, 0, SETVAL, MAX_VYROBKU);


    WORKER_NUM_SEM = semget(0x01010101, 1, IPC_CREAT | 0600);
    if (WORKER_NUM_SEM < 0) {
        perror("cannot create semaphore 2");
        exit(1);
    }
    semctl(WORKER_NUM_SEM, 0, SETVAL, 1);

    auto *data = (prepravka *) mmap(
            nullptr,
            sizeof(prepravka),
            PROT_READ | PROT_WRITE,
            MAP_SHARED | MAP_ANONYMOUS,
            -1,
            0
    );

    int childId = 1;
    for (int i = 0; i < WORKERS && childId; i++) {
        childId = fork();
    }

    if (childId) {
        task2_consumer(data);
        wait(NULL);

        semctl(WORKER_NUM_SEM, 0, IPC_RMID);
        semctl(CONSUMER_NUM_SEM, 0, IPC_RMID);
    } else {
        task2_worker(getpid(), data);
    }
}

sem_t *SEM_3_CONSUMER;
sem_t *SEM_3_WORKER;

void task3_worker(int pid, prepravka *crate) {
    for (int i = 0; i < GOODS_PER_WORKER; i++) {
        sem_wait(SEM_3_WORKER);
        int count = crate->pocet;
        crate->vyrobky[count++] = pid;
        crate->pocet = count;
        if (count < MAX_VYROBKU) {
            sem_post(SEM_3_WORKER);
        } else if (count == MAX_VYROBKU) {
            sem_post(SEM_3_CONSUMER);
        }
    }
}

void task3_consumer(prepravka *crate) {
    int maxCratesPerDay = (GOODS_PER_WORKER * WORKERS) / MAX_VYROBKU;

    for (int cratesCount = 0; cratesCount < maxCratesPerDay; cratesCount++) {
        sem_wait(SEM_3_CONSUMER);

        std::unordered_map<int, int> statistics;
        for (int i = 0; i < MAX_VYROBKU; i++) {
            int pid = crate->vyrobky[i];
            statistics[pid]++;
        }

        for (auto row: statistics) {
            std::cout << row.first << ": " << row.second << std::endl;
            if (row.first == 0) exit(-2);
        }

        crate->pocet = 0;
        memset(crate->vyrobky, 0, sizeof(crate->vyrobky));

        std::cout << "----------------------" << std::endl;

        sem_post(SEM_3_WORKER);

    }
}

void task3() {
    SEM_3_CONSUMER = (sem_t *) mmap(
            nullptr,
            sizeof(sem_t),
            PROT_READ | PROT_WRITE,
            MAP_SHARED | MAP_ANONYMOUS,
            -1,
            0
    );
    sem_init(SEM_3_CONSUMER, 1, 0);

    SEM_3_WORKER = (sem_t *) mmap(
            nullptr,
            sizeof(sem_t),
            PROT_READ | PROT_WRITE,
            MAP_SHARED | MAP_ANONYMOUS,
            -1,
            0
    );
    sem_init(SEM_3_WORKER, 1, 1);

    auto *data = (prepravka *) mmap(
            nullptr,
            sizeof(prepravka),
            PROT_READ | PROT_WRITE,
            MAP_SHARED | MAP_ANONYMOUS,
            -1,
            0
    );

    int childId = 1;
    for (int i = 0; i < WORKERS && childId; i++) {
        childId = fork();
    }

    if (childId) {
        task3_consumer(data);
        wait(NULL);
    } else {
        task3_worker(getpid(), data);
    }
}


int main() {
    //task1();
    //task2();
    //task3();
}