#include <iostream>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <poll.h>
#include <vector>
#include <algorithm>
#include <csignal>
#include <arpa/inet.h>
#include <cstring>

#include <openssl/rsa.h>       /* SSLeay stuff */
#include <openssl/x509.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#define TRY(x, y) { if (y < 0) { printf("code %d\n", y); perror(x); exit(254); } }
#define CHK_NULL(x) if ((x)==NULL) exit (1)
#define CHK_SSL(err) if ((err)==-1) { ERR_print_errors_fp(stderr); exit(2); }

#define HOME "./crts/"
#define CERTF  HOME "myserver.crt"
#define KEYF  HOME  "mypriv.pem"



/*
Upravte socket server ve verzi select nebo poll na verzi ssl serveru.
 */

SSL_CTX* initSSL() {
    const SSL_METHOD* meth;
    SSL_CTX* ctx;

    /* SSL preliminaries. We keep the certificate and key with the context. */

    SSL_load_error_strings();
    SSLeay_add_ssl_algorithms();
    meth = SSLv23_server_method();
    ctx = SSL_CTX_new(meth);
    if (!ctx) {
        ERR_print_errors_fp(stderr);
        exit(2);
    }

    if (SSL_CTX_use_certificate_file(ctx, CERTF, SSL_FILETYPE_PEM) <= 0) {
        ERR_print_errors_fp(stderr);
        exit(3);
    }
    if (SSL_CTX_use_PrivateKey_file(ctx, KEYF, SSL_FILETYPE_PEM) <= 0) {
        ERR_print_errors_fp(stderr);
        exit(4);
    }

    if (!SSL_CTX_check_private_key(ctx)) {
        fprintf(stderr, "Private key does not match the certificate public key\n");
        exit(5);
    }
    return ctx;
}

SSL* acceptSSLClient(int clientFd, SSL_CTX* ctx) {
    int err;
    int sd = clientFd;

    SSL* ssl = SSL_new(ctx);
    CHK_NULL(ssl);
    SSL_set_fd(ssl, sd);
    err = SSL_accept(ssl);
    CHK_SSL(err);
    return ssl;
}

int test() {

    int socketServerFileDescriptor = createNewTcpServer();

    std::vector<pollfd> polls;
    polls.push_back({socketServerFileDescriptor, POLLIN});

    while (true) {
        int pollRes = poll(polls.data(), polls.size(), 10000); // poll, size, timeout

        if (pollRes == 0) continue;
        for (auto &poll: polls) {
            if (poll.revents & POLLIN) {
                if (poll.fd == socketServerFileDescriptor) {
                    // handle new client
                    polls.push_back({socketAccept(socketServerFileDescriptor), POLLIN});
                } else {
                    // handle client message
                    std::cout << readClient(poll.fd) << std::endl;
                }
            }
        }
    }
}

int main() {
    SSL_CTX* ctx = initSSL();

    std::vector<pollfd> polls;
    std::vector<SSL*> sslClient;

    sockaddr_in inadr{};
    inadr.sin_family = AF_INET;
    inadr.sin_port = htons(12345);
    inadr.sin_addr.s_addr = INADDR_ANY;

    int sd = socket(AF_INET, SOCK_STREAM, 0);

    int opt = 1;
    TRY("reuse_port", setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)));
    TRY("bind", bind(sd, (sockaddr*) &inadr, sizeof(inadr)));
    TRY("listen", listen(sd, 100));

    polls.push_back({sd, POLLIN});
    sslClient.push_back(nullptr);

    while (true) {
        int pollRes = poll(polls.data(), polls.size(), 10000);
        if (pollRes < 0) TRY("poll", pollRes);
        if (pollRes == 0) {
            continue;
        }

        int i = -1;
        for (auto &poll: polls) {
            i++;
            if (poll.revents & POLLHUP) {
                poll.fd = -1;
                continue;
            }

            if (!(poll.revents & POLLIN)) {
                continue;
            }

            if (poll.fd == sd) { // server socket
                printf("new client\n");
                sockaddr_in peeradr{};
                socklen_t len_adr = sizeof(peeradr);
                int newsock = accept(sd, (sockaddr*) &peeradr, &len_adr);
                sslClient.push_back(acceptSSLClient(newsock, ctx));
                polls.push_back({newsock, POLLIN | POLLHUP | POLLNVAL});

            } else {
                printf("new message\n");
                char message[255];
                int size = SSL_read(sslClient[i], message, sizeof(message));
                CHK_SSL(size);

                if (size == 0) {
                    close(poll.fd);
                    poll.fd = -1;
                    continue;
                }

                std::string msg(message);
                if (msg.find("!close") != std::string::npos) {
                    close(poll.fd);
                    poll.fd = -1;
                    continue;
                }

                int i2 = 0;
                for (auto broadcastPoll: polls) {
                    if (!(broadcastPoll.fd == sd || broadcastPoll.fd == -1)) { // not the server socket
                        int writeRes = SSL_write(sslClient[i2], message, size);
                        CHK_SSL(writeRes)
                        if (writeRes < 0) {
                            broadcastPoll.fd = -1;
                        }
                    }
                    i2++;
                }
            }
        }

        auto pollIterator = polls.begin();
        auto sslClientIterator = sslClient.begin();
        for (int i = 0; i < polls.size(); ++i) {
            if (pollIterator->fd == -1) {
                SSL_free(*sslClientIterator);
                sslClientIterator = sslClient.erase(sslClientIterator);
                pollIterator = polls.erase(pollIterator);
            } else {
                pollIterator++;
                sslClientIterator++;
            }
        }
    }

    close(sd);
    SSL_CTX_free(ctx);
    return 0;
}